var moment = require('moment');

var formatDateToIso = function (date) {
    if(date == "danes") {
        let d = new Date().toISOString().split("T")[0];
        // console.log("DANES, d:", d);
        return d;
    }
    if( date == "jutri"){
        let today = new Date();
        let tomorrow = new Date(today)
        tomorrow.setDate(tomorrow.getDate() + 1);
        let d = new Date(tomorrow).toISOString().split("T")[0];
        // console.log("JUTRI, d:", d);
        return d;
    }
    if (date.split('-').length > 2) {
        return date;
    } else if (date.includes('.')) {
        let array = date.split(".");
        //add digit in front if it does not exsist
        array[0].length == 1 ? array[0] = '0' + array[0] : "";
        array[1].length == 1 ? array[1] = '0' + array[1] : "";
        // console.log("array in formatdatetoiso: ",array);
        // let m = moment(date);
        return `${array[2]}-${array[1]}-${array[0]}`;
        // console.log("m", m);
    }
}
var formatTime = function (time) {
    let array;
    // console.log(time);
    // if (time.split(':').length == 2) {
    //     return time;
    // }
    if (time.includes('.')) {
        array = time.split(".");
    } else {
        array = time.split(":");
    }
        //add digit in front if it does not exsist
        array[0].length == 1 ? array[0] = '0' + array[0] : "";
        array[1].length == 1 ? array[1] = '0' + array[1] : "";
        // console.log(array);
        // let m = moment(date);
        return `${array[0]}:${array[1]}`;
        // console.log("m", m);
}
var stringToTime = function(time, which_time){ //time = 'dopoldne' which_time= from/to
    if(which_time == 'from') {
        if(time === 'dopoldne' || 'dopoldan') {
            return '08:00';
        } else if(time == 'popoldne' || 'popoldan'){
            return '12:00';
        } else if(time == 'zvecer' || time == "zvečer") {
            return '18:00';
        } else {
            return "06:00"
        }
    } else {
        switch(time) {
            case 'dopoldne' || 'dopoldan':
                return '12:00';
            case 'popoldne' || 'popoldan': 
                return '19:00';
            case 'zvečer' || 'zvecer':
                return '23:00';
            default:
                return '23:00';
        }
    }
}
var getPastCarShares = function(subscriptions) {
    let currDate = new Date().toISOString().split("T")[0];
    let currTimestamp = Math.floor(new Date().getTime() / 1000);
    // { id: randomize('0', 4).toString(),
    // relation: `${location_from}-${location_to}`,
    // time: `${time_from}-${time_to}`, 
    // date: date, interval: interval }
    let pastCarShares = subscriptions.filter(el => {
        return dateToTimestamp(el.date, el.time.split('-')[1]) < currTimestamp;
    });
    // console.log("pastCarSHares", pastCarShares);
    return pastCarShares;
}
var dateToTimestamp = function(date, time) {
    let d = new Date(date + "T" + time).getTime() /1000 + 60*60;
    // console.log("dateTOTImestamp: ", d);
    return d;
}
// formatDateToIso('1.1.2002');
module.exports = { formatDateToIso, formatTime, getPastCarShares, stringToTime }
