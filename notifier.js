const TelegramBot = require('node-telegram-bot-api');
var axios = require('axios');
var config = require('./config.json').telegram;
var cheerio = require('cheerio');
var dateFunctions = require('./date');
var randomize = require('randomatic');
// var dateFunctions = require ('./date.js');
var currCarShares = [];
const token = "921958287:AAG0I2U4F3NDmrME_IOA_VaJ9BO0aqk_k08";
const bot = new TelegramBot(token, { polling: true });
var chatId;
var subscriptions = [];
const opts = {
    reply_markup:{
      keyboard: [
        // ['prevozi'],
        // ['prevoz']
      ]
    },
    parse_mode: 'Markdown'
  };
// console.log(config);
// Create a bot that uses 'polling' to fetch new updates
// var start = function(message) {
//     return new Promise((resolve, reject) => {
//         return bot.sendMessage(-1001462565134, message).then(res => {
//             // console.log("bot responsed:", res);
//             resolve("Telegram successfully posted! id:", res.message_id);
//         }).catch(err => {
//             // console.log(err);
//             reject(err);
//         });
//     });
// }
bot.on("polling_error", (err) => console.log(err));

bot.onText(/\/prevozi/, (msg, match) => {
    // console.log(msg)
    // console.log(match)
    const chatId = msg.chat.id;
    if (subscriptions.length == 0) {
        bot.sendMessage(chatId, `Nimaš aktivnih naročil! naročiš se lahko z ukazom prevoz`, opts);
    } else {
        bot.sendMessage(chatId, `trenutna naročila na prevoze: \n${getSubscriptions()}`, opts);
    }
})
bot.onText(/\/stop (.+)/, (msg, match) => {
    console.log(msg)
    console.log(match)
    const chatId = msg.chat.id;
    bot.sendMessage(chatId, `${stopSubscription(match[1])}`, opts);
})

bot.onText(/help/, (msg, match) => {
    // console.log(msg)
    // console.log(match)
    const chatId = msg.chat.id;
    bot.sendMessage(chatId, `Za obvestila o novih prevozih uporabi ukaz \n \n /prevoz <datum> <UraOd>-<UraDo> <relacijaOd>-<relacijaDo>
    \n Podprte so tudi ključne besede 'danes', 'jutri', 'zvecer', 'popoldne', 'dopoldne' \n\n Primer:`);
    bot.sendMessage(chatId, "/prevoz 1.1.2020 12:00-14:00 Ljubljana-Koper");
    bot.sendMessage(chatId, "/prevoz danes popoldne Kranj-Ljubljana");
    // prevoz 1.1.2002 Ljubljana-Koper 12:00-14:00
    bot.sendMessage(chatId, `Svoja naročila na prevoze lahko vidiš z ukazom /prevozi \n
    Na prevoz se lahko odjaviš z ukazom /stop <stevilkaPrevoza> \n
    primer:
    *7758* \t 2020-01-27 20:00-23:00 Ljubljana-Koper\n 
    Ukaz:
    /stop 7758
    `, opts);

})

bot.onText(/\/prevoz (.+)/, (msg, match) => {
    console.log('User:', msg.from.first_name, msg.from.last_name);
    //prepare data
    let params = match[1].split(" ");
    let date = dateFunctions.formatDateToIso(params[0]);
    let location_from = params[2].split("-")[0];
    let location_to = params[2].split("-")[1];
    let time_from;
    let time_to;
    if(!params[1].includes('-')){
        time_from = dateFunctions.stringToTime(params[1], "from");
        time_to = dateFunctions.stringToTime(params[1], "to"); 
    } else {
        time_from = dateFunctions.formatTime(params[1].split("-")[0]);
        time_to = dateFunctions.formatTime(params[1].split("-")[1]);
    }
    chatId = msg.chat.id;
    // console.log("NEKI:", neki);
    getCarShares(location_from, location_to, date, time_from, time_to)
        .then(carShares => {
            // console.log(carShares);
            let mapped = carShares.slice(0,15).map((el)=> {
                return formatCarShare(el);
            });
            let ids = carShares.map(el => { return el.id });
            // console.log("joined", mapped.join("\n"));
            let result = mapped.join("\n");
            if(mapped.length < 1) {
                bot.sendMessage(chatId, `Za izbrane parametre zaenkrat ni prevozov.`);
            }
            else {
                bot.sendMessage(chatId, `Ok, ${msg.from.first_name}. Dobil(a) boš obvestila o novih prevozih. `);
                bot.sendMessage(chatId, `Prevozi do sedaj za to uro na tej relaciji: \n${result}`);
            }
            startSubscription(location_from, location_to, date, time_from, time_to, ids);
        })
        .catch(err => {
            console.log("bot error,", err)
            bot.sendMessage(chatId, `Prišlo je do napake! Preveri, če so parametri ustrezni`)
        });
})

var getCarShares = function (location_from, location_to, date, time_from, time_to) {
    // console.log(location_from, location_to, date, time_from, time_to);
    // return "a";
    return fetchCarShares(location_from, location_to, date).then(res => {
        // console.log(res);
        if (typeof res == undefined) {
            return [];
        }
        if(res == undefined) {
            return [];
        }
        // console.log("res in getCarShares:", res);
        return res.filter(el => { return el.time.split(" ")[1] > time_from && el.time.split(" ")[1] < time_to });
    }).catch(err => console.log(err));
}
var fetchCarShares = function (location_from, location_to, date) {
    // console.log(location_from, location_to, date);
    return new Promise((resolve, reject) => {
        // console.log(`https://prevoz.org/api/search/shares/?f=${location_from}&fc=SI&t=${location_to}&tc=SI&d=${date}&exact=true&intl=false`);
        axios.get(`https://prevoz.org/api/search/shares/?f=${location_from}&fc=SI&t=${location_to}&tc=SI&d=${date}&exact=true&intl=false`)
            .then(function (response) {
                console.log("request sent!");
                // console.log(response.data.carshare_list)
                resolve(response.data.carshare_list);
                // console.log(response.data.carshare_list);
                // bot.sendMessage(-1001462565134, JSON.stringify(response.data.carshare_list[0])).then(res => {
                //     console.log("bot responsed:", res);
                //     // resolve("Telegram successfully posted! id:", res.message_id);
                // }).catch(err => {
                //     console.log(err);
                //     // reject(err);
                // });
            })
            .catch(function (error) {
                reject("ERROR 2", error);
            })
    })
}
var createInterval = function (location_from, location_to, date, time_from, time_to, ids) {
    let carSharesIds = ids; //saves id numbers of current available carshares
    return setInterval(() => {
        console.log("tick");
        getCarShares(location_from, location_to, date, time_from, time_to).then(carShares => {
            // console.log(carShares);
            let filtered = carShares.filter(el => { return !carSharesIds.includes(el.id) });
            if (filtered.length != 0) {
                let mapped = filtered.map(el => {
                    return formatCarShare(el);
                });
                // console.log("joined", mapped.join("\n"));
                let result = mapped.join("\n");
                bot.sendMessage(chatId, `Novi prevozi: \n${result}`);
                filtered.forEach(el => carSharesIds.push(el.id)); //add new carshares to exsisting array
            }
        }).catch(err => console.log(err));
    }, 5 * 1000 * 60);

}
var startSubscription = function (location_from, location_to, date, time_from, time_to, ids) {
    // console.log(randomize('0',4))
    subscriptions.push({ id: randomize('0', 4).toString(), relation: `${location_from}-${location_to}`, time: `${time_from}-${time_to}`, date: date, interval: createInterval(location_from, location_to, date, time_from, time_to, ids) })
}
var stopSubscription = function (id) {

    let index = subscriptions.map(function (e) { return e.id; }).indexOf(id);
    // console.log("INdex:", index);
    if (index == -1) {
        return "prevoz s to številko ne obstaja!";
    }
    clearInterval(subscriptions[index].interval);
    subscriptions.splice(index, 1);
    // console.log("subscriptions: ",subscriptions);
    return "Na prevoz si uspešno odjavljen(a)!";
}
getSubscriptions = function () {
    let mapped = subscriptions.map(el => {
        return `*${el.id}*\t${el.date} ${el.time} ${el.relation}`;
    });
    return mapped.join("\n");
}
endPastSubscriptions = function() {
    pastCarShares = dateFunctions.getPastCarShares(subscriptions);
    // console.log(pastCarShares);
    pastCarShares.forEach(el => {
        stopSubscription(el.id);
    })
}
var formatCarShare = function (el) {
    // console.log (el.author + "\t" + el.time.split(" ")[1] + "\t" + el.from + "\t" + el.to+ "\t" + el.contact);
    return `${el.author == "" ? "------" : el.author} ${el.time.split(" ")[1]} ${el.from}-${el.to}\t${el.contact}`;
}
var formatDate = function (date) {
    var d = new Date(date);
    return d.toISOString();
}

setInterval(endPastSubscriptions, 1000 * 60 * 60 * 3);
// setInterval(endPastSubscriptions, 1000 * 5);

// start();
// getCarShares();
// module.exports = {start};
